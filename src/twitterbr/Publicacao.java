package twitterbr;

import java.util.Date;

/**
 *
 * @author Anderson Lima
 */
public class Publicacao {
    private String Id;
    private String Descricao;
    public Date Data;

    public Publicacao(String Id, String Descricao) {
        this.Id = Id;
        this.Descricao = Descricao;
        this.Data = new Date();
    }

    public String getId() {
        return Id;
    }

    public String getDescricao() {
        return Descricao;
    }

    public String imprimir() {
         return "\nPOST - Id do user: " + this.getId() 
                 + " Descricao: " + this.Descricao 
                 + "\nData: " + this.Data 
                 +"\n----------------------------------------------------------------------------------------------";
    }
    
}
