package twitterbr;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Anderson Lima
 */
public class TwitterBR {

    public static void main(String[] args) {
        
        System.out.println("#######################################################");
        System.out.println("####- TwitterBR  - Drangon Ball Z -#####");
        
        HueTwitter hueTwt = new HueTwitter();
        
        Conta goku =  new Conta("Son Goku");
        hueTwt.PublicarBRTwitte(new Publicacao(goku.getId(), 
                "#homemMaisForteDoMundo #genkidama #kamehamehaaaa #gokuMaisForte u.u"), 
                goku);
        hueTwt.listaPulicacoesPorConta(goku);
        // hueTwt.retornaTwittesPorId(goku.getId()); // Mesma função do lista publicações por conta
        
        Conta vegeta =  new Conta("Vegeta");
        hueTwt.PublicarBRTwitte(new Publicacao(vegeta.getId(), 
                "#vegetaPrincipe #vegeta4ever #vegeteMaisFort #galickGun  :9"), vegeta);
        hueTwt.listaPulicacoesPorConta(vegeta);
        
        Conta buu = new Conta("Majin Boo");
        hueTwt.PublicarBRTwitte(new Publicacao(buu.getId(),
                "#vouTeTransformarEmChocolate #buuu #vouTeComer #vouTeComer"), 
                buu);
        hueTwt.listaPulicacoesPorConta(buu);
        
        Conta gohan = new Conta("Son Gohan");
        hueTwt.PublicarBRTwitte(new Publicacao(gohan.getId(),"Alguém viu meu papai?"), gohan);
        
        hueTwt.PublicarBRTwitte(new Publicacao(goku.getId(), "@" + gohan.getNome() + " Estou no planeta kaio, venha para cá treinar #todosSonJuntos #shallowNow"), goku);
        hueTwt.PublicarBRTwitte(new Publicacao(vegeta.getId(), "@" + goku.getNome() + " Toda essa loucura é mais de 8.000 :9 #maisDe8000 #loucuraTotal "), vegeta);
        hueTwt.PublicarBRTwitte(new Publicacao(gohan.getId(), "@" + goku.getNome() + " acaba com o " + "@" 
                                + vegeta.getNome() + " usa o #kayoken"), gohan);
        
        // Relatório final
        EscreverNoArquivo("todos-twitts.txt", hueTwt.ListaPublicacoes());
        
        EscreverNoArquivo(goku.getNome() +"-" + goku.getId()+"-twitts.txt", hueTwt.listaPulicacoesPorConta(goku));
        EscreverNoArquivo(vegeta.getNome()+"-" + vegeta.getId()+"-twitts.txt", hueTwt.listaPulicacoesPorConta(vegeta));
        EscreverNoArquivo(buu.getNome()+"-" + buu.getId()+"-twitts.txt", hueTwt.listaPulicacoesPorConta(buu));
        EscreverNoArquivo(gohan.getNome()+"-" + gohan.getId()+"-twitts.txt", hueTwt.listaPulicacoesPorConta(gohan));
       
    }
    public static void EscreverNoArquivo(String nomeArquivo, List<Publicacao> publicacoes) {
        try {
          String caminhoArquivo = "arquivos_texto/" +nomeArquivo;
          FileOutputStream fileOutPutStm = new FileOutputStream(caminhoArquivo);
          ObjectOutputStream ObjectOutPutStm = new ObjectOutputStream(fileOutPutStm);
          
          ObjectOutPutStm.writeObject("Arquivo:" + nomeArquivo);
          
          for(Publicacao post : publicacoes) {
              ObjectOutPutStm.writeObject(post.imprimir());
          }
          ObjectOutPutStm.close();
          fileOutPutStm.close();
          JOptionPane.showMessageDialog(null, "Arquivo gerado com sucesso em: " + caminhoArquivo);
        } catch(Exception ex) {
            JOptionPane.showMessageDialog(null, "Erro ao tentar gerar o arquivo.");
            System.out.println("Erro ao tentar salvar o arquivo. " + ex);
        }
    }
}
