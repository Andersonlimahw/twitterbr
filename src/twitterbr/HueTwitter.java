package twitterbr;

import java.awt.HeadlessException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Anderson Lima
 */
public class HueTwitter {
    public List<Conta> Contas = new ArrayList();
    public List<Publicacao> Publicacoes = new ArrayList();
    
    public void adicionarConta(Conta conta) {
        try {
            Contas.add(conta);
            JOptionPane.showMessageDialog(null, "Conta criada com sucesso!");
        } catch(HeadlessException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao criar conta!");
            System.out.println("Erro ao criar conta " + ex);
        }
       
    }
    /*
    * @param publicacao { Id: String, Descricao: String, Data: gerada}
      @param conta {Id: UUID, Nome: String, Publicacoes: Lista<Publicacao>}
      Adiciona publicacao a lista de publicações do HueTwitter
      Adiciona publicacação para a conta recebeida como param
    */
    public void PublicarBRTwitte(Publicacao publicacao, Conta conta) {
        try {
            Publicacoes.add(publicacao);
            conta.adicionaPublicacao(publicacao);
            JOptionPane.showMessageDialog(null, conta.getNome() + " Fez uma nova publicação");
        } catch(Exception ex) {
            JOptionPane.showMessageDialog(null, "Erro ao realizar publicação " + ex.getMessage());
        }
        
    }
    
    public List<Publicacao> retornaTwittesPorId(String Id) {
        List<Publicacao> publicacoesEncontradas = new ArrayList();
        if (!"".equals(Id.trim())) {
            System.out.println("Retornando publicacações para od Id: " + Id);
            for(Publicacao publicacao : Publicacoes) {
                if (publicacao.getId().equals(Id)) {
                    publicacoesEncontradas.add(publicacao);
                }
            }
        } else {
            JOptionPane.showMessageDialog(null, "Id para pesquisa não informado");
        }
        for(Publicacao post : publicacoesEncontradas) {
            System.out.println(post.imprimir());
        }
        return  publicacoesEncontradas; 
    }
    
    public List<Publicacao> listaPulicacoesPorConta(Conta conta) {
        List<Publicacao> publicacoesConta = new ArrayList();
        try {
            System.out.println("\nListando publicações para a conta : " + conta.imprimir());
            for(Publicacao post : conta.getPublicacoes()) {
                System.out.println(post.imprimir());
            }
            publicacoesConta = conta.getPublicacoes();
        } catch(Exception ex) {
            System.out.println("Erro ao recuperar as publicaações para " + conta.getNome() + " ex : " + ex);
        }
        return publicacoesConta;
    }
    
    public List<Publicacao> ListaPublicacoes() {
        List<Publicacao> todasPublicacoes = new ArrayList();
        try {
            System.out.println("\n####################################");
            System.out.println("\nListando todas as publicações");
            System.out.println("\n####################################");
            for(Publicacao post : Publicacoes) {
                System.out.println(post.imprimir());
                todasPublicacoes.add(post);
            }
            JOptionPane.showMessageDialog(null, "Foram encontradas " + Publicacoes.size() +" publicações");
            return todasPublicacoes;
        } catch(Exception ex) {
            System.out.println("Erro ao lista todas as publicações: " + ex);
            return todasPublicacoes;
        }
      
    }
    
}
