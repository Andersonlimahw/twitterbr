package twitterbr;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Anderson Lima
 */
public class Conta {
    private String Id;
    private String Nome;
    private List<Publicacao> Publicacoes = new ArrayList();

    public String getId() {
        return Id;
    }

    public Conta(String Nome) {
        this.Id = UUID.randomUUID().toString();
        this.Nome = Nome;
    }

    public String getNome() {
        return Nome;
    }

    public List<Publicacao> getPublicacoes() {
        return this.Publicacoes;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public void setPublicacoes(List<Publicacao> Publicacoes) {
        this.Publicacoes = Publicacoes;
    }

    void adicionaPublicacao(Publicacao publicacao) {
        System.out.println("Publicação adicionada para "+ this.getNome() + " Publicação: " + publicacao.getDescricao());
        Publicacoes.add(publicacao);
    }

    String imprimir() {
        return "Id: " + this.getId() +  " Nome: " + this.getNome();
    }
    
}
